from rest_framework.routers import DefaultRouter
from domitory import views

router = DefaultRouter()

router.register(r'managers', views.ManagerViewSet)
router.register(r'domitories', views.DomitoryViewSet)
router.register(r'bedrooms', views.BedroomViewSet)


app_name = 'domitory'

urlpatterns = router.urls
