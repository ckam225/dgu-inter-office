from rest_framework import serializers
from .models import Bedroom, Domitory, Manager
from core.shortcuts import PresentablePrimaryKeyRelatedField


class ManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manager
        fields = '__all__'


class DomitorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Domitory
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['manager'] = PresentablePrimaryKeyRelatedField(
            label='Manager', presentation_serializer=ManagerSerializer,
            queryset=Manager.objects.all(),
        )


class BedroomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bedroom
        fields = '__all__'
        read_only_fields = ('number',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['domitory'] = PresentablePrimaryKeyRelatedField(
            label='Domitory', presentation_serializer=DomitorySerializer,
            queryset=Domitory.objects.all(),
        )
