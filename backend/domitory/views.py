from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from .models import Bedroom, Domitory, Manager
from .serializers import (
    ManagerSerializer, DomitorySerializer, BedroomSerializer)
from . import filters


class ManagerViewSet(viewsets.ModelViewSet):
    queryset = Manager.objects.all()
    serializer_class = ManagerSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.ManagerFilter


class DomitoryViewSet(viewsets.ModelViewSet):
    queryset = Domitory.objects.all()
    serializer_class = DomitorySerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.DomitoryFilter


class BedroomViewSet(viewsets.ModelViewSet):
    queryset = Bedroom.objects.all()
    serializer_class = BedroomSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = filters.BedroomFilter
