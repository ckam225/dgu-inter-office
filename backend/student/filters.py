import django_filters
from . import models


class CountryFilter(django_filters.FilterSet):
    class Meta:
        model = models.Country
        fields = ['name', 'name_ru', 'e164', 'iso2']


class RegisterFilter(django_filters.FilterSet):
    class Meta:
        model = models.Register
        fields = ['state', 'level', 'scholarship',
                  'student', 'degree', 'program']


class StudentFilter(django_filters.FilterSet):
    class Meta:
        model = models.Student
        fields = ['name', 'citizen', 'placeofbirth', 'birthday', 'phone']


class VisaFilter(django_filters.FilterSet):
    class Meta:
        model = models.Visa
        fields = ['number', 'date_of_issue',
                  'expiry_at', 'delivery_place', 'student']


class PassportFilter(django_filters.FilterSet):
    class Meta:
        model = models.Passport
        fields = ['number', 'date_of_issue',
                  'expiry_at', 'delivery_place', 'student']


class CorrespondantFilter(django_filters.FilterSet):
    class Meta:
        model = models.Correspondant
        fields = ['name', 'type', 'student', 'birthday', 'phone']
