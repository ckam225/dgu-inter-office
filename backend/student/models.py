from django.db import models
from study.models import Scholarship, Degree, Program
from domitory.models import Bedroom
from core.shortcuts import upload_location
from django.utils import timezone


class Country(models.Model):
    class Meta:
        verbose_name = 'country'
        verbose_name_plural = 'countries'

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    name_ru = models.CharField(max_length=100,  null=True, blank=True)
    iso = models.CharField(max_length=3)
    iso2 = models.CharField(max_length=2)
    e164 = models.CharField(max_length=3)

    def __str__(self):
        return "{} - ({})".format(self.name, self.iso2)


class Student(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    birthday = models.DateField()
    placeofbirth = models.CharField(max_length=50)
    address = models.CharField(max_length=50, null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    avatar = models.ImageField(
        upload_to=upload_location, null=True, blank=True,
        default='default.png')
    citizen = models.ForeignKey(
        Country, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.name


class Passport(models.Model):
    id = models.AutoField(primary_key=True)
    number = models.CharField(max_length=25)
    date_of_issue = models.DateField()
    expiry_at = models.DateField()
    delivery_place = models.CharField(max_length=60, null=True, blank=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.number


class Correspondant(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    birthday = models.DateField(null=True, blank=True)
    type = models.CharField(max_length=50)
    address = models.CharField(max_length=50, null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Visa(models.Model):
    id = models.AutoField(primary_key=True)
    number = models.CharField(max_length=25)
    date_of_issue = models.DateField()
    expiry_at = models.DateField()
    delivery_place = models.CharField(max_length=60, null=True, blank=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.number


class Register(models.Model):
    id = models.AutoField(primary_key=True)
    state = models.IntegerField(default=0)
    level = models.IntegerField(default=1)
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    scholarship = models.ForeignKey(Scholarship, on_delete=models.CASCADE)
    degree = models.ForeignKey(Degree, on_delete=models.CASCADE)
    program = models.ForeignKey(Program, on_delete=models.CASCADE)

    def __str__(self):
        return "{} {} {} {}".format(self.student.name, self.scholarship.title,
                                    self.degree.title, self.program.title)

    def toJson(self):
        data = {
            'id': self.id,
            'student': self.student,
            'scholarship': self.scholarship,
            'degree': self.degree,
            'program': self.program,
        }
        return data


class Acomodation(models.Model):
    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    bedroom = models.ForeignKey(Bedroom, on_delete=models.CASCADE)
    scholarship = models.ForeignKey(
        Scholarship, on_delete=models.CASCADE, null=True,)
    paid = models.BooleanField(default=False,)

    def __str__(self):
        return "{}-{}".format(self.student.name, self.bedroom.number)
