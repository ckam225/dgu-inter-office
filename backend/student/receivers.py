
from django.db.models.signals import post_delete
from django.dispatch import receiver
from .models import Student


@receiver(post_delete, sender=Student)
def post_delete_student(sender, instance, **kwargs):
    instance.avatar.delete(False)