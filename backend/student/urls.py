from rest_framework.routers import DefaultRouter
from student import views

router = DefaultRouter()

router.register(r'students', views.StudentViewSet)
router.register(r'visas', views.VisaViewSet)
router.register(r'passports', views.PassportViewSet)
router.register(r'correspondants', views.CorrespondantViewSet)
router.register(r'registers', views.RegisterViewSet)
router.register(r'acommodations', views.AcomodationViewSet)
router.register('countries', views.CountryViewSet)


app_name = 'student'

urlpatterns = router.urls
