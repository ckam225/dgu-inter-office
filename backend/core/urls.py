from django.contrib import admin
from django.urls import path, include
from django.conf import settings
# from .routes import router
from .views import HomeView, APIRootView

urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('admin/', admin.site.urls),
    path('api/', APIRootView.as_view(), name='api-root'),
    # path('api/', include(router.urls), name='api-root'),
    path('api/', include('account.urls')),
    path('api/', include('domitory.urls')),
    path('api/', include('student.urls')),
    path('api/', include('study.urls')),
    path('api-auth/', include('rest_framework.urls')),
]


if settings.DEBUG:
    ''' api documentation '''

    from django.views.generic import TemplateView
    from rest_framework.schemas import get_schema_view

    schema_view = get_schema_view(
        title="DGU INTERNATION OFFICE PLATEFORM",
        description="API for all things …",

        # url='https://www.example.org/api/',
        version="1.0.0",
        urlconf='core.urls',
        patterns=urlpatterns
    )
    urlpatterns += [
        path('openapi', schema_view, name='openapi-schema'),
        path('docs/', TemplateView.as_view(
            template_name='docs/swagger.html',
            extra_context={'schema_url': 'openapi-schema'}
        ), name='swagger-ui'),
        path('redoc/', TemplateView.as_view(
            template_name='docs/redoc.html',
            extra_context={'schema_url': 'openapi-schema'}
        ), name='redoc'),
    ]

    ''' setup static folder '''
    from django.conf.urls.static import static

    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
