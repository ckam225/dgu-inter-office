from django.conf import settings
from django.views.generic import TemplateView
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView


class HomeView(TemplateView):
    """Рендерит главную страницу"""
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['on_debug'] = settings.DEBUG

        return context


class APIRootView(APIView):
    def get(self, request):
        return Response({
            'account': {
                'accounts': reverse('account:account-list', request=request),
                'register': reverse('account:account-register', request=request),
                'login': reverse('account:account-login', request=request),
                'current': reverse('account:account-current', request=request),
                'logout': reverse('account:account-logout', request=request),
            },
            'domitory': {
                'managers': reverse('domitory:manager-list', request=request),
                'domitories': reverse('domitory:domitory-list', request=request),
                'bedrooms': reverse('domitory:bedroom-list', request=request)
            },
            'study': {
                'scholarships': reverse('study:scholarship-list', request=request),
                'degrees': reverse('study:degree-list', request=request),
                'departments': reverse('study:department-list', request=request),
                'programs': reverse('study:program-list', request=request)
            },
            'student': {
                'students': reverse('student:student-list', request=request),
                'visas': reverse('student:visa-list', request=request),
                'passports': reverse('student:passport-list', request=request),
                'correspondants': reverse('student:correspondant-list', request=request),
                'registers': reverse('student:register-list', request=request),
                'acommodation': reverse('student:acomodation-list', request=request),
                'countries': reverse('student:country-list', request=request)
            },
        })
