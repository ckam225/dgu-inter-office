
import os
from datetime import datetime
from rest_framework.serializers import PrimaryKeyRelatedField
from collections import OrderedDict
from django.http import Http404


def load_module(module):
    try:
        mod = __import__(module)
        return mod
    except ModuleNotFoundError:
        raise ValueError("Module {} doesn't exist".format(module))


def module_exists(module):
    filename = os.path.join(os.getcwd(), "{}.py".format(
        str(module).replace('.', '/')))
    if os.path.exists(filename):
        return True
    return False


def get_int_or_404(value):
    if is_int(value):
        return int(value)

    raise Http404()


def get_request_param_or_404(request, param):
    my_param = None
    if request.method == 'GET':
        my_param = request.GET.get(param)
    if request.method == 'POST':
        my_param = request.POST.get(param)
    if request.method == 'PUT':
        my_param = request.PUT.get(param)
    if my_param is None:
        raise Http404()
    else:
        return my_param


def get_date_or_404(date):
    try:
        if datetime.strptime(date, '%Y-%m-%d'):
            return datetime(date)
        raise Http404()
    except ValueError:
        raise Http404()


def hash_str(str, digits=8):
    import hashlib
    hash_str = hashlib.sha1(str.encode())
    return hash_str.hexdigest()[:digits]


def upload_location(instance, filename):
    name_file, ext_file = os.path.splitext(filename)
    filename = "{0}{1}".format(hash_str(name_file, 16), ext_file)
    date_gen = "{:%y%m%d%H%M%S%z}".format(datetime.now())
    file_path = '{prefix}/{date}{filename}'.format(
        date=date_gen, filename=filename,
        prefix=instance.__class__.__name__.lower())
    return file_path


def validateEmail(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def validatePassword(password):
    import re
    # pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{8,20}$"
    pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,20}$"
    if re.match(pattern, password):
        return True
    else:
        return False


def get_password_errors():
    errors = [
        'Password can’t be too similar to your other personal information.',
        'Password must contain at least 8 characters.',
        'Password can’t be a commonly used password.',
        'Password can’t be entirely numeric.',
        'Password should have at least one lowercase and uppercase letter',
    ]
    return errors


def deserialize_object(object, status=200):
    from django.forms.models import model_to_dict
    import json

    data = model_to_dict(object)
    return json.dump(data)


def contains(str1, str2):
    if str1.lower() in str2.lower():
        return True
    return False


def is_int(val):
    return isinstance(parse_int(val), int)


def parse_int(val):
    try:
        return int(val)
    except ValueError:
        raise Http404()


def fake(filename, model, data=None, lines=5):
    path = os.path.join(os.getcwd(), str(
        model).split('.')[0], 'fixtures', filename)
    with open(path, 'w') as f:
        f.write("[\n")
        for line in range(lines):
            content = []
            content.append("\t{\n")
            content.append(f"\t\t\"model\":\"{model}\",\n")
            content.append("\t\t\"fields\": {\n")
            index = 0
            for key, value in data.items():
                index += 1
                content.append(f"\t\t\t\"{key}\":\"{value}\"")
                if index < len(data):
                    content.append(",\n")
                else:
                    content.append("\n")

            content.append("\t\t}\n")
            content.append("\t}")
            if line < lines-1:
                content.append(",\n")
            else:
                content.append("\n")

            for c in content:
                f.write(c)

        f.write("]\n")
        f.close()


def write_file(file, builder=None, erase=True):
    if builder is None:
        builder = []
    mode = "w" if erase else "a"
    try:
        with open(file, mode) as f:
            for content in builder:
                f.write(content)
            f.close()
    except Exception as e:
        print(e)


def write(path, dic):
    with open(path, 'w') as fp:
        content = str(dic).replace("'", '"')
        fp.write(content)
        fp.close()


def create_folder(path):
    if not os.path.exists(path):
        os.mkdir(path)
    else:
        print('{0} already exists in this project'.format(path))


def create_folders(paths):
    for path in paths:
        if not os.path.exists(path):
            os.mkdir(path)


def create_file(filename):
    if not os.path.exists(filename):
        open(filename, 'w')
    else:
        print('{0} already exists in this project'.format(filename))


def create_files(filenames):
    for filename in filenames:
        if not os.path.exists(filename):
            open(filename, 'w')
        else:
            print('{0} already exists in this project'.format(filename))


class PresentablePrimaryKeyRelatedField(PrimaryKeyRelatedField):
    """
    Override PrimaryKeyRelatedField to represent serializer data instead of a pk field of the object.
    """

    def use_pk_only_optimization(self):
        """
        Instead of sending pk only object, return full object. The object already retrieved from db by drf.
        This doesn't cause an extra query.
        It even might save from making an extra query on serializer.to_representation method.
        Related source codes:
        - https://github.com/tomchristie/django-rest-framework/blob/master/rest_framework/relations.py#L41
        - https://github.com/tomchristie/django-rest-framework/blob/master/rest_framework/relations.py#L132
        """
        return False

    def __init__(self, **kwargs):
        self.presentation_serializer = kwargs.pop(
            'presentation_serializer', None)
        assert self.presentation_serializer is not None, (
            'PresentablePrimaryKeyRelatedField must provide a `presentation_serializer` argument'
        )
        super(PresentablePrimaryKeyRelatedField, self).__init__(**kwargs)

    def get_choices(self, cutoff=None):
        queryset = self.get_queryset()
        if queryset is None:
            # Ensure that field.choices returns something sensible
            # even when accessed with a read-only field.
            return {}

        if cutoff is not None:
            queryset = queryset[:cutoff]

        return OrderedDict([(item.pk, self.display_value(item)) for item in queryset])

    def to_representation(self, data):
        return self.presentation_serializer(data, context=self.context).data
