from . import serializers
from .models import Account
from rest_framework.permissions import AllowAny
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from django.http import HttpResponse


class AccountViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all().order_by('-username')
    serializer_class = serializers.AccountListSerializer
    # permission_classes = [permissions.IsAdminUser]
    # renderer_classes = (renderers.AdminRenderer,)

    def __init__(self, *args, **kwargs):
        super(AccountViewSet, self).__init__(*args, **kwargs)
        self.serializer_action_classes = {
            'list': serializers.AccountListSerializer,
            'create': serializers.AccountRegisterSerializer,
            'retrieve': serializers.AccountRetrieveSerializer,
            'update': serializers.AccountUpdateSerializer,
        }

    def get_serializer_class(self, *args, **kwargs):
        """Instantiate the list of serializers per action from class attribute
        (must be defined)."""
        kwargs['partial'] = True
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super(AccountViewSet, self).get_serializer_class()

    def save_for_register(self, request):
        serializer = serializers.AccountRegisterSerializer(data=request.data)
        if serializer.is_valid():
            account = serializer.save()
            data = account.toJson()
            token = Token.objects.get(user=account)
            data['token'] = token.key
            return Response(data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        queryset = Account.objects.all().order_by('-full_name')
        serializer = serializers.AccountListSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        account = get_object_or_404(Account, pk=pk)
        serializer = serializers.AccountRetrieveSerializer(account)
        return Response(serializer.data)

    def create(self, request):
        return self.save_for_register(request)

    def update(self, request, pk=None):

        account = get_object_or_404(Account, pk=pk)
        serializer = serializers.AccountUpdateSerializer(
            account, data=request.data)
        if(serializer.is_valid()):
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def register(self, request):
        return self.save_for_register(request)

    @action(detail=False, methods=['post'], permission_classes=(AllowAny,))
    def login(self, request, pk=None):
        serializer = serializers.AccountAuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        content = user.toJson()
        content['token'] = token.key
        return Response(content)

    @action(detail=False, methods=['post', 'get'])
    def logout(self, request, pk=None):
        try:
            request.user.auth_token.delete()
            return HttpResponse(status=204)
        except (request.user.auth_token.ObjectDoesNotExist):
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    @action(detail=False, methods=['post', 'get'])
    def current(self, request, pk=None):
        try:
            serializer = serializers.AccountRetrieveSerializer(request.user)
            return Response(serializer.data)
        except (request.user.ObjectDoesNotExist):
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    @action(detail=True, methods=['post'])
    def reset_password(self, request, pk=None):
        account = get_object_or_404(Account, pk=pk)
        serializer = serializers.AccountResetPasswordSerializer(
            account, data=request.data)
        if(serializer.is_valid()):
            serializer.save(pk)
            return Response(serializer.data, status=status.HTTP_205_RESET_CONTENT)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
