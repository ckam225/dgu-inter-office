
from django.db.models import signals
from django.dispatch import receiver
from django.conf import settings
from rest_framework.authtoken.models import Token


@receiver(signals.post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(signals.pre_save, sender=settings.AUTH_USER_MODEL)
def pre_save_user(sender, instance=None, *args, **kwargs):
    print('******** pre_save_user receiver called ***********')
