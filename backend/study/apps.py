from django.apps import AppConfig
from core.shortcuts import load_module


class StudyConfig(AppConfig):
    name = 'study'

    def ready(self):
        super().ready()
        load_module('{}.receivers'.format(self.name))
