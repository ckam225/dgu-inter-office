from django.db.models import signals
from django.dispatch import receiver
from .models import Scholarship


@receiver(signals.pre_save, sender=Scholarship)
def pre_save_scholarship(sender, instance=None, *args, **kwargs):
    print('******** pre_save_scholarship receiver has been called ***********')
    if(instance.start_at >= instance.end_at):
        raise ValueError('start_at must be less than end_at')
    instance.title = "{} - {}".format(instance.start_at.year,
                                      instance.end_at.year)


@receiver(signals.post_save, sender=Scholarship)
def post_save_scholarship(sender, instance=None, created=False, **kwargs):
    print('******** post_save_scholarship receiver has been called ***********')
