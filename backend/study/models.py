from django.db import models
from django.utils import timezone


class Scholarship(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=15, blank=True, null=True)
    start_at = models.DateField()
    end_at = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.title


class Department(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=160)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.title


class Program(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=160)
    code = models.CharField(max_length=30)
    description = models.TextField(blank=True, null=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return f"{self.code} - {self.title}"


class Degree(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=160)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def __str__(self):
        return self.title
