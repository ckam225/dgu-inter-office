import django_filters
from . import models


class ScholarshipFilter(django_filters.FilterSet):
    class Meta:
        model = models.Scholarship
        fields = ['title', 'start_at', 'end_at']


class DepartmentFilter(django_filters.FilterSet):
    class Meta:
        model = models.Department
        fields = ['title']


class ProgramFilter(django_filters.FilterSet):
    class Meta:
        model = models.Program
        fields = ['title', 'code', 'department']


class DegreeFilter(django_filters.FilterSet):
    class Meta:
        model = models.Degree
        fields = ['title']
