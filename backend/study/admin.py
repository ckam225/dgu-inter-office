from django.contrib import admin
from .models import Scholarship, Department, Degree, Program
# Register your models here.


class ScholarshipAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'start_at', 'end_at', 'created_at')
    readonly_fields = ('title',)


admin.site.register(Scholarship, ScholarshipAdmin)
admin.site.register(Degree)
admin.site.register(Department)
admin.site.register(Program)
