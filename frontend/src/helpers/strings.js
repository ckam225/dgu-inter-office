export function truncate(str, maxChar) {
  if (str.length > maxChar) {
    const res = str.slice(0, maxChar + 1);
    return res + "...";
  }
  return str;
}

/**
 * Capitalize first character
 * @param {*} str character to capitalize
 * @return {*} string value
 */
export function capitalize(str) {
  return str && str[0].toUpperCase() + str.slice(1);
}
