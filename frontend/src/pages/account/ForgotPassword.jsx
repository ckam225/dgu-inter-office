import React from "react";
import Layout from "../../layouts/Auth";
import logo from "../../assets/svg/logo.svg";
import Page from "../../components/shared/Page";

export default () => {
  return (
    <Layout>
      <Page transition="slide-left">
        <div className="form-content">
          <form className="form">
            <div
              className="flex flex-column flex-align-center"
              style={{ marginBottom: "10px" }}
            >
              <img src={logo} alt="" width="150" height="150" />
            </div>
            <div className="flex flex-justify-content-center">
              <span
                className="smart-text"
                style={{
                  fontSize: "24px",
                  color: "#4F4F4F",
                  marginBottom: "10px",
                  fontWeight: "bold",
                }}
              >
                Забыли пароль?
              </span>
            </div>
            <div>
              <input
                className="text-field"
                type="email"
                placeholder="Email"
                style={{ marginBottom: "10px", marginTop: "10px" }}
                required={true}
              />
              <div align="right">
                <a href="/account/login" className="smrt-link">
                  Войти
                </a>
              </div>
              {/* <div align="center" v-if="submitted">
                <ProgressBar />
            </div> */}
              <button className="flat-btn" type="submit">
                Отправить
              </button>
            </div>
          </form>
        </div>
      </Page>
    </Layout>
  );
};
