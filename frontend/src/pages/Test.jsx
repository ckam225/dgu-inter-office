import React, { useRef, useState } from "react";
import CountryPicker from "../components/elements/CountryPicker";
import CountryField from "../components/elements/CountryField";
import Page from "../components/shared/Page";

const Test = () => {
  const cpRef = useRef(null);
  const [country, setCountry] = useState(null);

  function handlerSelectedCountry(c) {
    setCountry(c);
  }

  return (
    <div>
      <Page>
        <h1>Page de tests:</h1>
        <CountryField
          width="200"
          country={country}
          onClick={() => cpRef.current.show()}
        />
        <CountryPicker refId={cpRef} onSelected={handlerSelectedCountry} />
      </Page>
    </div>
  );
};

export default Test;
