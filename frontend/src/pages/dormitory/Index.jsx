import React from "react";
import Layout from "../../layouts/Main";
import Page from "../../components/shared/Page";

export default () => {
  return (
    <Layout title="Domitory ...">
      <Page transition="zoom">
        <h1>Domitory</h1>
      </Page>
    </Layout>
  );
};
