import React from "react";
import Layout from "../../layouts/Blank";

export default () => {
  return (
    <Layout title="404">
      <div className="area-error">
        <h1 className="error-404">404</h1>
        <h1>Эта страница не может быть найдена.</h1>
        <button className="btn-44">Назад</button>
      </div>
    </Layout>
  );
};
