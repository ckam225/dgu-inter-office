import React from "react";
import Layout from "../../layouts/Main";
import Page from "../../components/shared/Page";

export default () => {
  return (
    <Layout title="VISA ...">
      <Page>
        <div className="panel">
          <div className="panel-header">
            <span className="panel-title">Визы</span>
            <div>
              <button icon="plus" pt="5" pb="5">
                Оформлять
              </button>
            </div>
          </div>
          <div className="panel-body">
            <table className="table panel-body">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Студент</th>
                  <th>Дата выдачи</th>
                  <th>Срок действия</th>
                  <th>Оставшиеся дни</th>
                  <th>progress</th>
                  <th></th>
                </tr>
              </thead>
              {/* <tbody>
                <tr>
                  <td colspan="6" align="center">
                    <Loader color="#59218C"/>
                  </td>
                </tr>
              </tbody> */}
              <tbody>
                {/* <visa-item v-for="(visa, i) in visas" :key="i" :visa="visa" /> */}
              </tbody>
            </table>
          </div>
        </div>
      </Page>
    </Layout>
  );
};
