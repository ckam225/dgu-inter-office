import api from "./";
import storage from "../store/storage";

class Scholarship {
  get current() {
    return storage.get("scholarship");
  }

  set current(scholarship) {
    storage.set("scholarship", scholarship);
  }

  async getAll() {
    try {
      const res = await api.get(`/scholarships/`);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async getById(id) {
    try {
      const res = await api.get(`/scholarships/${id}/`);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async create(credentials) {
    try {
      const res = await api.post(`/scholarships/`, credentials);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async update(credentials, id) {
    try {
      const res = await api.put(`/scholarships/${id}/`, credentials);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async delete(id) {
    try {
      const res = await api.delete(`/scholarships/${id}/`);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }
}

export default new Scholarship();
