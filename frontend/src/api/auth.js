import storage from "../store/storage";
import api from "./";
class Auth {
  async login(credentials) {
    try {
      const response = await api.post("/accounts/login/", credentials);
      storage.set("token", response.data.token);
      storage.set("user", response.data);
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async register(credentials) {
    try {
      const res = await api.post("/accounts/register/", credentials);
      return { error: false, result: res.data, status: res.status };
    } catch (error) {
      return {
        error: true,
        result: error.response.data,
        status: error.response.status,
      };
    }
  }

  async current() {
    try {
      const response = await api.post("/accounts/current/");
      storage.set("token", response.data.token);
      storage.set("user", response.data);
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async logout() {
    try {
      const response = await api.post("/accounts/logout/");
      storage.reset();
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async users() {
    try {
      const response = await api.get(`/accounts/`);
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  async getById(id) {
    try {
      const response = await api.get(`/accounts/current/${id}`);
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  isAuthenticated() {
    if (storage.exists("token")) {
      return true;
    }
    return false;
  }

  getLocalUser() {
    return storage.get("user");
  }
}

export default new Auth();
