import api from "./index";

class Student {
  static async getById(id) {
    try {
      const response = await api.get(`/students/${id}/`);
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async create(credentials) {
    try {
      const res = await api.post(`/students/`, credentials);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async update(credentials, id) {
    try {
      const res = await api.put(`/students/${id}/`, credentials);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async delete(id) {
    try {
      const res = await api.delete(`/students/${id}/`);
      return api.format(res);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async getCurrentRegisters(scholarship, limit, offset, name = null) {
    try {
      let params = {
        scholarship: scholarship.id,
        offset,
        limit,
      };
      if (name !== null) {
        params = { ...params, name: name };
      }
      const response = await api.get(`/students/recent_registers/`, { params });
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async getRegisters(student, scholarship = null) {
    try {
      let params = {
        student: student,
      };
      if (scholarship !== null) {
        params = { ...params, scholarship };
      }
      const response = await api.get(`/registers/`, { params });
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async getVisas(student) {
    try {
      const params = {
        student: student,
      };
      const response = await api.get(`/visas/`, { params });
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async getCorrespondants(student) {
    try {
      const params = {
        student: student,
      };
      const response = await api.get(`/correspondants/`, { params });
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }

  static async getPassports(student) {
    try {
      const params = {
        student: student,
      };
      const response = await api.get(`/passports/`, { params });
      return api.format(response);
    } catch (error) {
      return api.format(error.response, true);
    }
  }
}

export default Student;
