import React from "react";
import Sidebar from "../components/shared/Sidebar";
import TopBar from "../components/shared/Topbar";
import Breadcrumb from "../components/shared/Breadcrumb";
export default ({ children, breadcrumbs = [] }) => {
  return (
    <div className="area-main">
      <Sidebar />
      <div className="wrapper">
        <TopBar />
        <Breadcrumb breadcrumbs={breadcrumbs} />
        {children}
      </div>
    </div>
  );
};
