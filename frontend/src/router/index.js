import React from "react";
import { Router } from "@reach/router";
import { ProtectedRoute, AuthRoute, Route, NestedRoute } from "./routes";

import HomePage from "../pages/Home";
import NotFoundPage from "../pages/error/404";
import VisaPage from "../pages/visa/Index";
import DomitoryPage from "../pages/dormitory/Index";
import SettingsPage from "../pages/settings/Index";
import StudentPage from "../pages/student/Index";
import StudentDetailsPage from "../pages/student/Details";
import LoginPage from "../pages/account/Login";
import ForgotPasswordPage from "../pages/account/ForgotPassword";
import DevToolsPage from "../pages/DevTools";
import TestsPage from "../pages/Test";

export default () => {
  return (
    <Router>
      <ProtectedRoute path="/" page={HomePage} />
      <NestedRoute path="/student">
        <ProtectedRoute path="/" page={StudentPage} />
        <ProtectedRoute path=":studentId" page={StudentDetailsPage} />
      </NestedRoute>
      <ProtectedRoute path="/visa" page={VisaPage} />
      <ProtectedRoute path="/domitory" page={DomitoryPage} />
      <ProtectedRoute path="/settings" page={SettingsPage} />
      <AuthRoute path="/account/login" page={LoginPage} />
      <Route path="/account/forgotpassword" page={ForgotPasswordPage} />
      <Route path="/devtools" page={DevToolsPage} />
      <Route path="/tests" page={TestsPage} />
      <Route default page={NotFoundPage} />
    </Router>
  );
};
