import React from "react";
import { Redirect } from "@reach/router";
import auth from "../api/auth";

export const ProtectedRoute = ({ page: Component, ...rest }) => {
  const Route = (props) => {
    if (auth.isAuthenticated()) {
      return <Component {...props} {...rest} />;
    } else {
      return <Redirect to="/account/login" from={props.location} noThrow />;
    }
  };
  return <Route />;
};

export const AuthRoute = ({ page: Component, ...rest }) => {
  const Route = (props) => {
    if (auth.isAuthenticated()) {
      return <Redirect to="/" from={props.location} noThrow />;
    } else {
      return <Component {...props} {...rest} />;
    }
  };
  return <Route />;
};

export const Route = ({ page: Component, ...rest }) => {
  const BaseRoute = (props) => {
    return <Component {...props} {...rest} />;
  };
  return <BaseRoute />;
};

export const NestedRoute = (props) => {
  return <>{props.children}</>;
};
