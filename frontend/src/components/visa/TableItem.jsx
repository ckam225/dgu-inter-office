import React from "react";

const TableItem = ({ visa }) => {
  return (
    <tr v-if="visa">
      <td>{visa.number}</td>
      <td>
        <a href="`/students/${visa.student.id}`">{visa.student.name}</a>
      </td>
      <td>{visa.date_of_issue}</td>
      <td>{visa.expiry_at}</td>
      <td>
        {{ spend }}/<b>{{ estimate }}</b>
      </td>
      <td>
        {/* <Progression :progress="percent" :title="`${percent}%`" /> */}
      </td>
      <td align="center">
        {/* <IconButton icon="edit" size="32" />
      <IconButton icon="remove" size="32" /> */}
      </td>
    </tr>
  );
};

export default TableItem;
