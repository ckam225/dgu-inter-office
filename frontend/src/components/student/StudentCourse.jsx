import React from "react";

export default ({ registers = [] }) => {
  return (
    <div className="panel">
      <div className="panel-title">Курсы & Образования</div>
      <div className="panel-body">
        <table className="table no-hover">
          <tbody>
            {registers.map((register, k) => (
              <tr key={k}>
                <td>{register.scholarship.title}</td>
                <td>{register.level}-й курс</td>
                <td>{register.program.department.title}</td>
                <td>{register.degree.title}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
