import React from "react";

export default ({ visas = [] }) => {
  return (
    <div className="panel">
      <div className="panel-title">Визы</div>
      <div className="panel-body">
        <table className="table no-hover">
          <thead>
            <tr>
              <th>№</th>
              <th>Дата выдачи</th>
              <th>Срок действия</th>
              <th>Выдан</th>
            </tr>
          </thead>
          <tbody>
            {visas.map((visa, k) => (
              <tr key={k}>
                <td>{visa.number}</td>
                <td>{visa.date_of_issue}</td>
                <td>{visa.expiry_at}</td>
                <td>{visa.delivery_place}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
