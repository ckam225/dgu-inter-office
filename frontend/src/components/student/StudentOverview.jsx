import React from "react";
import Flag from "react-country-flag";
import { truncate } from "../../helpers/strings";

export default ({ student, register }) => {
  return (
    <div className="panel" style={{ width: "300px" }}>
      <div className="panel-title">
        O{" "}
        {student && (
          <span title={student.name}>{truncate(student.name, 20)}</span>
        )}
      </div>
      <div className="panel-body">
        <div style={{ textAlign: "center" }}>
          {student && (
            <img
              src={student.avatar}
              style={{
                background: "#ddd",
                width: "250px",
                height: "210px",
              }}
              alt={student.name}
              title={student.name}
            />
          )}
        </div>
        <ul className="list-item">
          {register && (
            <li>
              {/* <Icon name="graduate" size="24" fill="#b2b2b2" /> */}
              <div>
                <b>
                  {register.level}-й курс /{" "}
                  <span title={register.program.title}>
                    {truncate(register.program.title, 15)}
                  </span>
                </b>
                <br />
                <span
                  className="ml-2"
                  title={register.program.department.title}
                >
                  {truncate(register.program.department.title, 25)}
                </span>
              </div>
            </li>
          )}
          {student && (
            <li>
              <Flag
                countryCode={student.citizen.iso2}
                svg
                style={{
                  width: "1em",
                  height: "1em",
                  marginRight: "3px",
                }}
              />
              <span className="ml-2">{student.citizen.name}</span>
            </li>
          )}
          {student && (
            <li>
              {/* <Icon name="mobile" size="24" fill="#b2b2b2" /> */}
              <span className="ml-2">{student.phone}</span>
            </li>
          )}
        </ul>
      </div>
    </div>
  );
};
