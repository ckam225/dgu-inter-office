import React from "react";
import StudentItem from "./StudentItem";

import Loader from "../elements/Loading";

export default ({ onSelected, loading, students = [] }) => {
  return (
    <div className="component">
      <table className="table">
        <thead>
          <tr>
            <th>Фамилия</th>
            <th>Образования</th>
            <th>Направление</th>
            <th>Уровень</th>
            <th>Курс</th>
            <th>Страна</th>
          </tr>
        </thead>
        {loading ? (
          <tbody>
            <tr>
              <td colSpan="6" align="center" className="no-hover">
                <Loader align="center" />
              </td>
            </tr>
          </tbody>
        ) : students.length > 0 ? (
          <tbody>
            {students.map((stud, k) => {
              return (
                <StudentItem dataset={stud} key={k} onSelected={onSelected} />
              );
            })}
          </tbody>
        ) : (
          <tbody>
            <tr>
              <td colSpan="6" align="center" className="no-hover">
                <span>No data available</span>
              </td>
            </tr>
          </tbody>
        )}
      </table>
    </div>
  );
};
