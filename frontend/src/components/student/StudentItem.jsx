import React from "react";
import PropTypes from "prop-types";
import Flag from "react-country-flag";
import { truncate } from "../../helpers/strings";

const StudentItem = ({ dataset, onSelected }) => {
  return (
    <tr className="item" onClick={() => onSelected(dataset.student.id)}>
      <td>{dataset.student.name}</td>
      <td>
        <span>{dataset.scholarship.title}</span>
      </td>
      <td>
        <span>{truncate(dataset.program.title, 25)}</span>
      </td>
      <td>
        <span>{dataset.degree.title}</span>
      </td>
      <td>
        <span>{dataset.level}-й курс</span>
      </td>
      <td>
        <div className="flex flex-align-center">
          <Flag
            countryCode={dataset.student.citizen.iso2}
            key={dataset.student.citizen.iso2}
            svg
            style={{
              width: "2em",
              height: "2em",
              marginRight: "3px",
            }}
          />
          <span className="ml-2">{dataset.student.citizen.name}</span>
        </div>
      </td>
    </tr>
  );
};

StudentItem.prototypes = {
  dataset: PropTypes.object.isRequired,
};

export default StudentItem;
