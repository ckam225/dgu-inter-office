import React, { useState } from "react";
import icon from "../../assets/svg/icons.svg";

const SearchField = ({ onChange, placeholder, width }) => {
  const [value, setSalue] = useState("");

  function handlerChange(e) {
    setSalue(e.target.value);
    onChange(e.target.value);
  }
  return (
    <label className="search-input-label">
      <input
        type="search"
        className="search-input"
        placeholder={placeholder}
        value={value}
        style={{ width: `${width}px` || "100%" }}
        onChange={handlerChange}
        autoFocus
      />
      <svg className="search-input-icon">
        <use xlinkHref={`${icon}#search`} />
      </svg>
    </label>
  );
};

export default SearchField;
