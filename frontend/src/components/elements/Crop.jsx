import React from "react";

export default ({ children, color, size, corner, mode = "circle" }) => {
  return (
    <div
      className="crop"
      style={{
        width: `${size}px` || "32px",
        height: `${size}px` || "32px",
        background: color || "#f5f5f5",
        borderRadius: mode === "square" ? `${corner}px` : "50%" || 0,
      }}
    >
      {children}
    </div>
  );
};
