import React from "react";

export default ({
  message,
  size,
  orientation,
  align,
  valign,
  isFull = false,
}) => {
  const getSize = size ? `${size}px` : "40px";
  const verticalAlign = valign ? valign : "center";
  return (
    <div
      className={`progress-content${isFull ? " full" : ""}`}
      style={
        orientation === "vertical"
          ? { flexDirection: "column", justifyContent: "center" }
          : {
              flexDirection: "row",
              alignItems: verticalAlign,
              justifyContent: align,
            }
      }
    >
      <div
        className="progress-bar"
        style={{ width: getSize, height: getSize }}
      />
      {message && <span className="progress-msg">{message}</span>}
    </div>
  );
};
