import React from "react";
import icon from "../../assets/svg/icons.svg";

export default ({ name, color, style, size = 24 }) => {
  return (
    <svg fill={color} width={size} height={size} style={style}>
      <use xlinkHref={`${icon}#${name}`} />
    </svg>
  );
};
