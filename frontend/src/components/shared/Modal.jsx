import React, { useImperativeHandle, useRef } from "react";

export default ({ children, title, width, refId, closable = true }) => {
  const modal = useRef(null);

  const closeHandler = () => {
    modal.current.classList.remove("open");
    modal.current.classList.add("hidden");
    setTimeout(() => {
      modal.current.classList.remove("hidden");
    }, 200);
  };

  useImperativeHandle(refId, () => ({
    modal,
    open() {
      modal.current.classList.add("open");
    },
    close() {
      closeHandler();
    },
  }));

  return (
    <div className="modal" ref={modal}>
      <div className="modal-overlay">
        <div className="modal-content" style={{ width: `${width || 600}px` }}>
          <div className="modal-header">
            <span className="modal-title">{title}</span>
            {closable && (
              <span className="modal-close" onClick={closeHandler}>
                &times;
              </span>
            )}
          </div>
          <div className="modal-body">{children}</div>
        </div>
      </div>
    </div>
  );
};
