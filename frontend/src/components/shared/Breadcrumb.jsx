import React from "react";
import { useLocation } from "@reach/router";
import Icon from "../elements/Icon";
import { capitalize } from "../../helpers/strings";

const Breadcrumb = () => {
  const [breadcrumbs, setBreadcrumbs] = React.useState([]);
  const location = useLocation();

  React.useEffect(() => {
    if (location.pathname === "/") {
      setBreadcrumbs([{ name: "Главная", path: "/" }]);
    } else {
      const paths = location.pathname.split("/");
      const routes = [];
      paths.slice(1, paths.length).forEach((route) => {
        routes.push({ name: capitalize(route), path: route });
      });
      setBreadcrumbs(routes);
    }
  }, []);

  return (
    <nav className="breadcrumb">
      {breadcrumbs.map((route, i) => (
        <div key={i} className="breadcrumb__item">
          {i > 0 || i === breadcrumbs.length - 1 ? (
            <span>{route.name}</span>
          ) : (
            <a href={`/${route.path}`}>{route.name}</a>
          )}
          {i < breadcrumbs.length - 1 && (
            <Icon
              name="chevron_right"
              size={14}
              color="#000"
              style={{ margin: "0 5px" }}
            />
          )}
        </div>
      ))}
    </nav>
  );
};

export default Breadcrumb;
