import React, { useState, useImperativeHandle } from "react";

const Popup = ({ children, title, width, refId, closable = true }) => {
  const [visible, setVisible] = useState(false);

  useImperativeHandle(refId, () => ({
    open() {
      setVisible(true);
    },
    close() {
      setVisible(false);
    },
  }));
  const closeHandler = () => {
    setVisible(false);
  };
  return (
    <div className={`popup ${visible ? "visible" : ""}`} ref={refId}>
      <div className="popup-content" style={{ width: `${width || 600}px` }}>
        <div className="popup-header">
          <span className="popup-title">{title}</span>
          {closable && (
            <span className="popup-close" onClick={closeHandler}>
              &times;
            </span>
          )}
        </div>
        <div className="popup-body">{children}</div>
      </div>
    </div>
  );
};
export default Popup;
