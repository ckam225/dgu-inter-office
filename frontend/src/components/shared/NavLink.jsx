// import React from "react";
// import { Link } from "@reach/router";

// const NavLink = (props) => {
//   return (
//     <Link
//       {...props}
//       getProps={({ isCurrent }) => {
//         // the object returned here is passed to the
//         // anchor element's props
//         return {
//           className: isCurrent ? "active" : "",
//         };
//       }}
//     />
//   );
// };

// export default NavLink;

import React from "react";
import { Link, Match } from "@reach/router";

export const NavLink = ({ to, exact, ...rest }) => (
  <Match path={`${to}/*`}>
    {({ location }) => {
      const isActive = exact
        ? location.pathname === to
        : location.pathname.includes(to);
      return <Link to={to} className={isActive ? "active" : ""} {...rest} />;
    }}
  </Match>
);

export const BaseLink = ({
  to,
  className,
  activeClass,
  inactiveClass,
  exact,
  ...rest
}) => (
  <Match path={`${to}/*`}>
    {({ location }) => {
      const isActive = exact
        ? location.pathname === to
        : location.pathname.includes(to);
      const allClass =
        className + (isActive ? ` ${activeClass}` : ` ${inactiveClass}`);
      return <Link to={to} className={allClass} {...rest} />;
    }}
  </Match>
);

export default NavLink;
