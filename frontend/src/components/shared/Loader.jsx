import React from "react";

export default () => {
  return (
    <div
      style={{
        zIndex: "99999",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "rgba(0,0,0,0.2)",
        position: "fixed",
        top: "0",
        left: "0",
        bottom: "0",
        right: "0",
      }}
    >
      <div className="lds-roller">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};
